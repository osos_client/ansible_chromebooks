#!/bin/bash

apt update -y
apt install ansible -y

/usr/bin/ansible-pull -i "localhost" -e ansible_python_interpreter=/usr/bin/python3 -d /var/lib/ansible -U https://gitlab.com/osos_client/ansible_chromebooks.git
